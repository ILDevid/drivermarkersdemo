package it.ildevid.markers.model;

import java.io.Serializable;

public class RoutePoint implements Serializable {

	private LocationData previousLocation;
	private LocationData currentLocation;

	public RoutePoint() {
	}

	public RoutePoint(LocationData previousLocation, LocationData currentLocation) {
		this.previousLocation = previousLocation;
		this.currentLocation = currentLocation;
	}

	public LocationData getPreviousLocation() {
		return previousLocation;
	}

	public void setPreviousLocation(LocationData previousLocation) {
		this.previousLocation = previousLocation;
	}

	public LocationData getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(LocationData currentLocation) {
		this.currentLocation = currentLocation;
	}

}
