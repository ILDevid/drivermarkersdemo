package it.ildevid.markers.model;

import android.graphics.Bitmap;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.io.Serializable;

import it.ildevid.markers.BR;
import it.ildevid.markers.DriverMarkersApplication;
import it.ildevid.markers.R;
import it.ildevid.markers.utils.BitmapUtils;

public class PoiData extends BaseObservable implements Serializable {

	private long id;
	private transient Bitmap image;
	private String note;

	private LocationData location;

	public PoiData() {
		reset();
	}

	public void reset() {
		this.image = null;
		this.note = "";
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public void setImage(String base64Image) {
		this.image = BitmapUtils.base64ToBitmap(base64Image);
	}

	@Bindable
	public String getNote() {
		if (!TextUtils.isEmpty(note)) {
			return note;
		} else {
			return DriverMarkersApplication.getContext().getString(R.string.empty_note);
		}
	}

	public void setNote(String note) {
		if (!this.note.equals(note)) {
			this.note = note;
			notifyPropertyChanged(BR.note);
		}
	}

	public LocationData getLocation() {
		return location;
	}

	public void setLocation(LocationData location) {
		this.location = location;
	}

	@NonNull
	@Override
	public String toString() {
		return "PoiData{" +
				"id=" + id +
				", image=" + image +
				", note='" + note + '\'' +
				", locationData=" + location +
				'}';
	}

}
