package it.ildevid.markers.model;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class LocationData implements Serializable {

	private double latitude;
	private double longitude;

	public LocationData() {
	}

	public LocationData(LatLng position) {
		this.latitude = position.latitude;
		this.longitude = position.longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@NonNull
	@Override
	public String toString() {
		return "LocationData{" +
				"latitude=" + latitude +
				", longitude=" + longitude +
				'}';
	}

}
