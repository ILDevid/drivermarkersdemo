package it.ildevid.markers.viewmodel;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import it.ildevid.markers.callable.GetPoiListTask;
import it.ildevid.markers.callable.SavePoiTask;
import it.ildevid.markers.callable.SaveRouteTask;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.model.RoutePoint;
import it.ildevid.markers.utils.Constants;

public class MapViewModel extends ViewModel {

	private static final String TAG = MapViewModel.class.getSimpleName();

	private MutableLiveData<Boolean> savePoiResponse;
	private MutableLiveData<Boolean> saveRouteResponse;
	private MutableLiveData<ArrayList<PoiData>> poiListResponse;
	private MutableLiveData<RoutePoint> mapRouteLiveData;

	private LocationData lastLocation;
	private ArrayList<PoiData> poiList;
	private HashMap<Long, PoiData> poiMap;
	private PoiData currentPoi;
	private ObservableBoolean navigationActive;
	private ArrayList<LocationData> currentRoute;

	private final static String LAST_LOCATION_VALUE = TAG + "LastLocationValue";
	private final static String POI_LIST_VALUE = TAG + "PoiListValue";
	private final static String POI_MAP_VALUE = TAG + "PoiMapValue";
	private final static String CURRENT_POI_VALUE = TAG + "CurrentPoiValue";
	private final static String CURRENT_PATH_VALUE = TAG + "CurrentPathValue";

	public MapViewModel() {
		reset();
	}

	public void reset() {
		this.savePoiResponse = new MutableLiveData<>();
		this.saveRouteResponse = new MutableLiveData<>();
		this.poiListResponse = new MutableLiveData<>();
		this.mapRouteLiveData = new MutableLiveData<>();
		this.lastLocation = null;
		this.poiList = new ArrayList<>();
		this.poiMap = new HashMap<>();
		this.currentPoi = new PoiData();
		this.navigationActive = new ObservableBoolean(false);
		this.currentRoute = new ArrayList<>();
	}

	public void restoreSaveInstanceState(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			lastLocation = (LocationData) savedInstanceState.getSerializable(LAST_LOCATION_VALUE);
			poiList = (ArrayList<PoiData>) savedInstanceState.getSerializable(POI_LIST_VALUE);
			poiMap = (HashMap<Long, PoiData>) savedInstanceState.getSerializable(POI_MAP_VALUE);
			currentPoi = (PoiData) savedInstanceState.getSerializable(CURRENT_POI_VALUE);
			currentRoute = (ArrayList<LocationData>) savedInstanceState.getSerializable(CURRENT_PATH_VALUE);
		}
	}

	public void onSaveInstanceState(@NonNull Bundle outState) {
		outState.putSerializable(LAST_LOCATION_VALUE, lastLocation);
		outState.putSerializable(POI_LIST_VALUE, poiList);
		outState.putSerializable(POI_MAP_VALUE, poiMap);
		outState.putSerializable(CURRENT_POI_VALUE, currentPoi);
		outState.putSerializable(CURRENT_PATH_VALUE, currentRoute);
	}

	public LocationData getLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(LocationData location) {
		if (getNavigationActive().get()) {
			mapRouteLiveData.postValue(new RoutePoint(this.lastLocation, location));
		}
		this.lastLocation = location;
		getCurrentPoi().setLocation(location);
	}

	public ArrayList<PoiData> getPoiListCached() {
		return poiList;
	}

	public PoiData getCurrentPoi() {
		return currentPoi;
	}

	public void setCurrentPoi(PoiData currentPoi) {
		this.currentPoi = currentPoi;
	}

	public void resetCurrentPoi() {
		this.currentPoi = new PoiData();
	}

	public MutableLiveData<Boolean> savePoi() {
		Single.fromCallable(new SavePoiTask(currentPoi, Constants.SYNCHRONIZATION_ENABLED))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(Schedulers.io())
				.subscribe(new SingleObserver<Boolean>() {
					@Override
					public void onSubscribe(@NonNull Disposable disposable) {
						//
					}

					@Override
					public void onSuccess(@NonNull Boolean result) {
						savePoiResponse.setValue(result);
					}

					@Override
					public void onError(@NonNull Throwable throwable) {
						Log.e(TAG, "savePoi task error: " + throwable.getMessage());
						savePoiResponse.setValue(false);
					}
				});
		return savePoiResponse;
	}

	public MutableLiveData<ArrayList<PoiData>> getPoiList() {
		Single.fromCallable(new GetPoiListTask())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribeOn(Schedulers.io())
				.subscribe(new SingleObserver<ArrayList<PoiData>>() {
					@Override
					public void onSubscribe(@NonNull Disposable disposable) {
						//
					}

					@Override
					public void onSuccess(@NonNull ArrayList<PoiData> result) {
						poiList = result;
						poiMap = createPoiMap(poiList);
						poiListResponse.setValue(result);
					}

					@Override
					public void onError(@NonNull Throwable throwable) {
						Log.e(TAG, "getPoiList task error: " + throwable.getMessage());
						poiListResponse.setValue(new ArrayList<>());
					}
				});
		return poiListResponse;
	}

	private HashMap<Long, PoiData> createPoiMap(ArrayList<PoiData> poiList) {
		HashMap<Long, PoiData> mapData = new HashMap<>();
		for (PoiData item : poiList) {
			mapData.put(item.getId(), item);
		}
		return mapData;
	}

	public HashMap<Long, PoiData> getPoiMap() {
		return poiMap;
	}

	public ObservableBoolean getNavigationActive() {
		return navigationActive;
	}

	public void setNavigationActive(boolean navigationActive) {
		this.navigationActive.set(navigationActive);
	}

	public void toggleNavigationActive() {
		this.navigationActive.set(!navigationActive.get());
	}

	public void addPointToCurrentPath(LocationData point) {
		this.currentRoute.add(point);
	}

	public ArrayList<LocationData> getCurrentRoute() {
		return currentRoute;
	}

	public MutableLiveData<RoutePoint> getMapRouteLiveData() {
		return mapRouteLiveData;
	}

	public MutableLiveData<Boolean> stopRouteTrackingAndSaveData(boolean saveRoute) {
		if (saveRoute) {
			Single.fromCallable(new SaveRouteTask(currentRoute, Constants.SYNCHRONIZATION_ENABLED))
					.observeOn(AndroidSchedulers.mainThread())
					.subscribeOn(Schedulers.io())
					.subscribe(new SingleObserver<Boolean>() {
						@Override
						public void onSubscribe(@NonNull Disposable disposable) {
							//
						}

						@Override
						public void onSuccess(@NonNull Boolean success) {
							currentRoute.clear();
							saveRouteResponse.setValue(success);
						}

						@Override
						public void onError(@NonNull Throwable throwable) {
							Log.e(TAG, "stopRouteTrackingAndSaveData task error: " + throwable.getMessage());
							saveRouteResponse.setValue(false);
						}
					});
		} else {
			currentRoute.clear();
		}
		return saveRouteResponse;
	}

}
