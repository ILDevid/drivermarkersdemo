package it.ildevid.markers.viewmodel;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WindowViewModel extends ViewModel {

	private static final String TAG = WindowViewModel.class.getSimpleName();

	private final MutableLiveData<Integer> loader;

	Handler timeOut = new Handler(Looper.getMainLooper());
	Runnable runnableTimeout = this::hideLoader;

	public WindowViewModel() {
		loader = new MutableLiveData<>(0);
	}

	public MutableLiveData<Integer> getLoader() {
		return loader;
	}

	public synchronized boolean isShowingLoader() {
		return loader.getValue() != null && loader.getValue() != 0;
	}

	public synchronized void showLoader() {
		Log.d(TAG, "loader value = " + loader.getValue());
		int old = loader.getValue() != null ? loader.getValue() : 0;
		loader.setValue(old + 1);
	}

	public synchronized void showLoader(int timeout) {
		Log.d(TAG, "loader value = " + loader.getValue());
		int old = loader.getValue() != null ? loader.getValue() : 0;
		loader.setValue(old + 1);
		timeOut.postDelayed(runnableTimeout, timeout);
	}

	public synchronized void hideLoader() {
		Log.d(TAG, "loader value = " + loader.getValue());
		int value = loader.getValue() - 1;
		if (value >= 0) {
			loader.setValue(value);
		}
	}

}
