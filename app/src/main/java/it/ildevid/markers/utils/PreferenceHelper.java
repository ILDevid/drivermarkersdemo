package it.ildevid.markers.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;

import it.ildevid.markers.model.LocationData;

public class PreferenceHelper {

	private static final String LAST_LOCATION_DATA = "a";

	private static PreferenceHelper instance;

	public static PreferenceHelper getInstance() {
		if (instance == null) {
			instance = new PreferenceHelper();
		}
		return instance;
	}

	public synchronized void putLastLocationData(Context context, LocationData location) {
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
		Gson gson = new Gson();
		editor.putString(LAST_LOCATION_DATA, gson.toJson(location));
		editor.apply();
	}

	public synchronized LocationData getLastLocationData(Context context) {
		Gson gson = new Gson();
		String json = PreferenceManager.getDefaultSharedPreferences(context).getString(LAST_LOCATION_DATA, "");
		LocationData value = null;
		if (!TextUtils.isEmpty(json)) {
			value = gson.fromJson(json, LocationData.class);
		}
		return value;
	}

}
