package it.ildevid.markers.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class DeviceUtils {

	public static boolean isDeviceOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = null;
		if (cm != null) {
			netInfo = cm.getActiveNetworkInfo();
		}
		return netInfo != null && netInfo.isConnected();
	}

}
