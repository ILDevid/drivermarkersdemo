package it.ildevid.markers.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;

public class BitmapUtils {

	public static String bitmapToBase64(@NonNull Bitmap bitmap) {
		if (bitmap != null) {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
			byte[] byteArray = byteArrayOutputStream.toByteArray();
			return Base64.encodeToString(byteArray, Base64.DEFAULT);
		}
		return "";
	}

	public static Bitmap base64ToBitmap(@NonNull String base64Image) {
		String cleanImage = base64Image.replace("data:image/png;base64,", "").replace("data:image/jpeg;base64,","");
		byte[] decodedString = Base64.decode(cleanImage, Base64.DEFAULT);
		return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
	}

}
