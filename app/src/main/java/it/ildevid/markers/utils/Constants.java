package it.ildevid.markers.utils;

import java.util.concurrent.TimeUnit;

public class Constants {

	public static final String BASE_URL = "http://drivermarkers.test.eu/demo/";
	public static final int NETWORK_TIMEOUT = 60;

	public static final boolean SYNCHRONIZATION_ENABLED = false;

	public static final int UPDATE_INTERVAL = (int) TimeUnit.SECONDS.toMillis(2);
	public static final int FASTEST_INTERVAL = (int) TimeUnit.SECONDS.toMillis(1);
	public static final int DISPLACEMENT = 10; //10 meters

}
