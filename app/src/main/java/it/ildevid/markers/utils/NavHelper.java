package it.ildevid.markers.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.NavAction;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import it.ildevid.markers.R;

public class NavHelper {

	private static final String TAG = NavHelper.class.getSimpleName();

	public static void navigate(FragmentActivity activity, NavDirections directions) {

		int popUpTo = -1;
		boolean isPopUpToInclusive = false;
		boolean isLaunchSingleTop = false;

		NavController navController = Navigation.findNavController(activity, R.id.nav_host_fragment);
		if (navController.getCurrentDestination() != null) {
			NavAction action = navController.getCurrentDestination().getAction(directions.getActionId());
			if (action != null) {
				NavOptions navOptions = action.getNavOptions();
				if (navOptions != null) {
					popUpTo = navOptions.getPopUpTo();
					isPopUpToInclusive = navOptions.isPopUpToInclusive();
					isLaunchSingleTop = navOptions.shouldLaunchSingleTop();
				}
			}
		}

		NavOptions.Builder builder;


		builder = new NavOptions.Builder()
				.setEnterAnim(R.anim.fragment_open_enter)
				.setExitAnim(R.anim.fragment_open_exit)
				.setPopEnterAnim(R.anim.fragment_close_enter)
				.setPopExitAnim(R.anim.fragment_close_exit)
				.setLaunchSingleTop(isLaunchSingleTop);

		if (popUpTo != -1) {
			builder.setPopUpTo(popUpTo, isPopUpToInclusive);
		}

		navigate(activity, directions, builder.build());
	}

	public static void navigate(NavController navController, NavDirections directions) {

		int popUpTo = -1;
		boolean isPopUpToInclusive = false;
		boolean isLaunchSingleTop = false;

		if (navController.getCurrentDestination() != null) {
			NavAction action = navController.getCurrentDestination().getAction(directions.getActionId());
			if (action != null) {
				NavOptions navOptions = action.getNavOptions();
				if (navOptions != null) {
					popUpTo = navOptions.getPopUpTo();
					isPopUpToInclusive = navOptions.isPopUpToInclusive();
					isLaunchSingleTop = navOptions.shouldLaunchSingleTop();
				}
			}
		}
		NavOptions.Builder builder = new NavOptions.Builder()
				.setEnterAnim(R.anim.fragment_open_enter)
				.setExitAnim(R.anim.fragment_open_exit)
				.setPopEnterAnim(R.anim.fragment_close_enter)
				.setPopExitAnim(R.anim.fragment_close_exit)
				.setLaunchSingleTop(isLaunchSingleTop);

		if (popUpTo != -1) {
			builder.setPopUpTo(popUpTo, isPopUpToInclusive);
		}

		navigate(navController, directions, builder.build());
	}

	public static void navigateNoTransition(NavController navController, NavDirections directions) {

		int popUpTo = -1;
		boolean isPopUpToInclusive = false;
		boolean isLaunchSingleTop = false;


		if (navController.getCurrentDestination() != null) {
			NavAction action = navController.getCurrentDestination().getAction(directions.getActionId());
			if (action != null) {
				NavOptions navOptions = action.getNavOptions();
				if (navOptions != null) {
					popUpTo = navOptions.getPopUpTo();
					isPopUpToInclusive = navOptions.isPopUpToInclusive();
					isLaunchSingleTop = navOptions.shouldLaunchSingleTop();
				}
			}
		}
		NavOptions.Builder builder = new NavOptions.Builder()
				.setLaunchSingleTop(isLaunchSingleTop);

		if (popUpTo != -1) {
			builder.setPopUpTo(popUpTo, isPopUpToInclusive);
		}

		navigate(navController, directions, builder.build());
	}

	private static void navigate(Activity activity, NavDirections directions, NavOptions navOptions) {
		if (activity != null) {
			NavController navController = Navigation.findNavController(activity, R.id.nav_host_fragment);
			navigate(navController, directions, navOptions);
		}
	}

	private static void navigate(NavController navController, NavDirections directions, NavOptions navOptions) {
		navigate(navController, directions,  navOptions, 4);
	}


	private static void navigate(NavController navController, NavDirections directions, NavOptions navOptions, int numRetry) {

		if (navController.getCurrentDestination() != null) {
			if (navController.getCurrentDestination().getAction(directions.getActionId()) != null) {
				navController.navigate(directions, navOptions);
				numRetry = 0;
			}
		}

		if(numRetry > 0){
			Log.d(TAG, "retry " + numRetry);
			numRetry--;
			Handler handler = new Handler(Looper.getMainLooper());
			int finalNumRetry = numRetry;
			handler.postDelayed(() -> navigate(navController, directions, navOptions, finalNumRetry), 500);
		}

	}

	public static void popBackStack(Activity activity) {
		if (activity != null) {
			Navigation.findNavController(activity, R.id.nav_host_fragment).popBackStack();
		}
	}

	public static void popBackStack(Activity activity, @IdRes int destinationId, boolean inclusive) {
		if (activity != null) {
			Navigation.findNavController(activity, R.id.nav_host_fragment).popBackStack(destinationId, inclusive);
		}
	}

	public static Fragment getForegroundFragment(@NonNull AppCompatActivity activity) {
		Fragment navHostFragment = activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
		return navHostFragment == null ? null : navHostFragment.getChildFragmentManager().getFragments().get(0);
	}

}
