package it.ildevid.markers.database;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import it.ildevid.markers.model.LocationData;

public class Converters {

	@TypeConverter
	public static ArrayList<LocationData> fromString(String value) {
		Type listType = new TypeToken<ArrayList<LocationData>>() {}.getType();
		return new Gson().fromJson(value, listType);
	}

	@TypeConverter
	public static String fromArrayList(ArrayList<LocationData> list) {
		Gson gson = new Gson();
		return gson.toJson(list);
	}

}
