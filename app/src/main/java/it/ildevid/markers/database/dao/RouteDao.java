package it.ildevid.markers.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import it.ildevid.markers.database.entity.RouteEntity;

@Dao
public interface RouteDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	long saveRoute(RouteEntity route);

	@Query("select * from route_table order by route_id collate nocase asc")
	List<RouteEntity> getRouteList();

	@Query("select * from route_table where route_id = :routeId")
	RouteEntity getRoute(long routeId);

	@Query("delete from route_table")
	void clear();

}
