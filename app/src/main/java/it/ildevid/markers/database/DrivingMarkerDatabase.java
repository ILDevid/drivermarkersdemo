package it.ildevid.markers.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import it.ildevid.markers.database.dao.PoiDao;
import it.ildevid.markers.database.dao.RouteDao;
import it.ildevid.markers.database.entity.PoiEntity;
import it.ildevid.markers.database.entity.RouteEntity;

@Database(entities = {PoiEntity.class, RouteEntity.class},
		version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class DrivingMarkerDatabase extends RoomDatabase {

	private static final String DB_NAME = "driving_marker_db";
	private static DrivingMarkerDatabase instance;

	public static synchronized DrivingMarkerDatabase getInstance(Context context) {
		if (instance == null) {
			instance = Room.databaseBuilder(context.getApplicationContext(), DrivingMarkerDatabase.class, DB_NAME)
					.fallbackToDestructiveMigration()
					.build();
		}

		return instance;
	}

	public abstract PoiDao getPoiDao();

	public abstract RouteDao getRouteDao();

}
