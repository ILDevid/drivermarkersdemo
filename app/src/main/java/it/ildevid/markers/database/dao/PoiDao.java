package it.ildevid.markers.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import it.ildevid.markers.database.entity.PoiEntity;

@Dao
public interface PoiDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	long savePoi(PoiEntity poi);

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void savePoiList(List<PoiEntity> poiList);

	@Query("update poi_table set poi_image = :image and poi_note = :note where poi_id = :poiId")
	void updatePoi(long poiId, String image, String note);

	@Query("select count (*) from poi_table")
	int getPoiNumber();

	@Query("select * from poi_table order by poi_id collate nocase asc")
	List<PoiEntity> getPoiList();

	@Query("select * from poi_table where poi_id = :poiId")
	PoiEntity getPoi(long poiId);

	@Query("delete from poi_table")
	void clear();

}
