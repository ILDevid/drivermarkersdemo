package it.ildevid.markers.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = PoiEntity.DataEntity.TABLE_NAME)
public class PoiEntity implements Serializable {

	@PrimaryKey(autoGenerate = true)
	@ColumnInfo(name = DataEntity.COLUMN_NAME_POI_ID)
	protected long poiId;

	@ColumnInfo(name = DataEntity.COLUMN_NAME_POI_IMAGE)
	protected String imageBase64;

	@ColumnInfo(name = DataEntity.COLUMN_NAME_POI_NOTE)
	protected String note;

	@ColumnInfo(name = DataEntity.COLUMN_NAME_POI_LATITUDE)
	protected double latitude;

	@ColumnInfo(name = DataEntity.COLUMN_NAME_POI_LONGITUDE)
	protected double longitude;

	public long getPoiId() {
		return poiId;
	}

	public void setPoiId(long poiId) {
		this.poiId = poiId;
	}

	public String getImageBase64() {
		return imageBase64;
	}

	public void setImageBase64(String imageBase64) {
		this.imageBase64 = imageBase64;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@NonNull
	@Override
	public String toString() {
		return "PoiEntity{" +
				"poiId=" + poiId +
				", imageBase64='" + imageBase64 + '\'' +
				", note='" + note + '\'' +
				", latitude=" + latitude +
				", longitude=" + longitude +
				'}';
	}

	static class DataEntity {
		static final String TABLE_NAME = "poi_table";
		static final String COLUMN_NAME_POI_ID = "poi_id";
		static final String COLUMN_NAME_POI_IMAGE = "poi_image";
		static final String COLUMN_NAME_POI_NOTE = "poi_note";
		static final String COLUMN_NAME_POI_LATITUDE = "poi_latitude";
		static final String COLUMN_NAME_POI_LONGITUDE = "poi_longitude";
	}

}
