package it.ildevid.markers.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;

import it.ildevid.markers.model.LocationData;

@Entity(tableName = RouteEntity.DataEntity.TABLE_NAME)
public class RouteEntity implements Serializable {

	@PrimaryKey(autoGenerate = true)
	@ColumnInfo(name = DataEntity.COLUMN_NAME_ROUTE_ID)
	protected long routeId;

	@ColumnInfo(name = DataEntity.COLUMN_NAME_ROUTE_POINTS)
	protected ArrayList<LocationData> routePoints;

	public long getRouteId() {
		return routeId;
	}

	public void setRouteId(long routeId) {
		this.routeId = routeId;
	}

	public ArrayList<LocationData> getRoutePoints() {
		return routePoints;
	}

	public void setRoutePoints(ArrayList<LocationData> routePoints) {
		this.routePoints = routePoints;
	}

	@NonNull
	@Override
	public String toString() {
		return "RouteEntity{" +
				"routeId=" + routeId +
				", routePoints=" + routePoints +
				'}';
	}

	static class DataEntity {
		static final String TABLE_NAME = "route_table";
		static final String COLUMN_NAME_ROUTE_ID = "route_id";
		static final String COLUMN_NAME_ROUTE_POINTS = "route_points";
	}

}
