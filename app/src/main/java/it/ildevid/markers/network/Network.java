package it.ildevid.markers.network;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.ConnectException;

import it.ildevid.markers.DriverMarkersApplication;
import it.ildevid.markers.network.dto.DtoAppResponse;
import it.ildevid.markers.network.exception.HttpException;
import it.ildevid.markers.network.exception.NoConnectionException;
import it.ildevid.markers.network.exception.StatusException;
import retrofit2.Call;
import retrofit2.Response;

import static it.ildevid.markers.network.NetworkResponseStatus.SUCCESS;

public class Network<T> {

	private final Call<T> call;

	public Network(Call<T> call) {
		this.call = call;
	}

	public T call() throws Exception {
		ConnectivityManager cm = (ConnectivityManager) DriverMarkersApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null || cm.getActiveNetworkInfo() == null || !cm.getActiveNetworkInfo().isConnectedOrConnecting()) {
			throw new NoConnectionException();
		}
		Response<T> response;

		try {
			response = call.execute();
		} catch (ConnectException ce) {
			throw new NoConnectionException();
		}

		if (!response.isSuccessful() || response.body() == null) {
			throw new HttpException(response.message(), response.code());
		} else if (response.body() instanceof DtoAppResponse) {
			String status = ((DtoAppResponse<?>) response.body()).getStatus();
			if (!status.equals(SUCCESS)) {
				throw new StatusException(status);
			}
		}

		return response.body();
	}

}
