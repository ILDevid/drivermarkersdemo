package it.ildevid.markers.network.dto;

import androidx.annotation.NonNull;

import java.io.Serializable;

import it.ildevid.markers.network.NetworkResponseStatus;

public class DtoAppResponse<T> implements Serializable {

    protected T res;
    @NetworkResponseStatus.Status
    protected String status;

    public T getRes() {
        return res;
    }

    public void setRes(T res) {
        this.res = res;
    }

    @NetworkResponseStatus.Status
    public String getStatus() {
        return status;
    }

    public void setStatus(@NetworkResponseStatus.Status String status) {
        this.status = status;
    }

    @NonNull
    @Override
    public String toString() {
        return "DtoAppResponse{" +
                "res=" + res +
                ", dtoStatus=" + status +
                '}';
    }

}

