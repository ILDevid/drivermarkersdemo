package it.ildevid.markers.network.exception;

public class NoConnectionException extends Exception {

    public NoConnectionException() {
        super("Internet not available");
    }
}
