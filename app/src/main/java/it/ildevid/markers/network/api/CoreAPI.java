package it.ildevid.markers.network.api;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import it.ildevid.markers.BuildConfig;
import it.ildevid.markers.network.NetworkInterceptor;
import it.ildevid.markers.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoreAPI {

	private static ICoreAPI SERVER_API;

	private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
	private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
	public static final Executor THREAD_POOL_EXECUTOR = Executors.newFixedThreadPool(CORE_POOL_SIZE);

	private Retrofit getRetrofit() {
		return new Retrofit.Builder()
				.client(getClient())
				.baseUrl(Constants.BASE_URL)
				.callbackExecutor(THREAD_POOL_EXECUTOR)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
	}

	private OkHttpClient getClient() {
		OkHttpClient.Builder builder =  new OkHttpClient.Builder()
				.connectTimeout(Constants.NETWORK_TIMEOUT, TimeUnit.SECONDS)
				.writeTimeout(Constants.NETWORK_TIMEOUT, TimeUnit.SECONDS)
				.readTimeout(Constants.NETWORK_TIMEOUT, TimeUnit.SECONDS);

		builder.addInterceptor(new NetworkInterceptor());

		if (BuildConfig.DEBUG) {
			HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
			interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
			builder.addInterceptor(interceptor);
		}

		return builder.build();
	}


	public static ICoreAPI getServerAPI() {
		if (SERVER_API == null) {
			SERVER_API = new CoreAPI().getRetrofit().create(ICoreAPI.class);
		}
		return SERVER_API;
	}

	private CoreAPI() {
	}

}

