package it.ildevid.markers.network.dto;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class DtoAppRequest<T> implements Serializable {

    private T req;

    public DtoAppRequest(T req){
        this.req = req;
    }

    public T getReq() {
        return req;
    }

    public void setReq(T req) {
        this.req = req;
    }

    @NonNull
    @Override
    public String toString() {
        return "DtoAppRequest{" +
                "req=" + req +
                '}';
    }

}
