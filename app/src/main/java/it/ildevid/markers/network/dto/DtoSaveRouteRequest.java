package it.ildevid.markers.network.dto;

import java.io.Serializable;
import java.util.List;

public class DtoSaveRouteRequest implements Serializable {

	private String id;
	private List<DtoLocation> points;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<DtoLocation> getPoints() {
		return points;
	}

	public void setPoints(List<DtoLocation> points) {
		this.points = points;
	}

}
