package it.ildevid.markers.network;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.IOException;

import it.ildevid.markers.network.dto.DtoAppRequest;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

public class NetworkInterceptor implements Interceptor {

	public static final String TAG = NetworkInterceptor.class.getSimpleName();

	@NonNull
	@Override
	public Response intercept(Chain chain) throws IOException {
		DtoAppRequest<?> appRequest = getDtoAppRequest(chain.request());
		String stringRequestBody = new Gson().toJson(appRequest);
		return networkCall(stringRequestBody, chain);
	}

	private DtoAppRequest<?> getDtoAppRequest(Request request) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Buffer buffer = new Buffer();
		request.body().writeTo(buffer);
		String originRequestBody = buffer.readUtf8();

		String prettyRequestBody = gson.toJson(gson.fromJson(originRequestBody, JsonObject.class));
		Log.d(TAG, "Plain Request Body:\n" + prettyRequestBody);

		return new Gson().fromJson(originRequestBody, DtoAppRequest.class);
	}

	private Response networkCall(String stringRequestBody, Chain chain) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		Request request = chain.request();

		MediaType contentType = request.body().contentType();

		RequestBody requestBody = RequestBody.create(stringRequestBody, contentType);
		Request.Builder requestBuilder = request.newBuilder();

		request = requestBuilder
				.post(requestBody)
				.build();

		Response response = chain.proceed(request);

		if (response.isSuccessful()) {
			ResponseBody responseBody = response.body();
			BufferedSource source = responseBody.source();
			source.request(Long.MAX_VALUE);
			Buffer buffer = source.getBuffer();
			String stringResponseBody = buffer.readUtf8();
			try {
				String prettyResponseBody = gson.toJson(gson.fromJson(stringResponseBody, JsonObject.class).get("dtoAppResponse"));

				Log.d(TAG, "Response Body:\n" + prettyResponseBody);
				responseBody = ResponseBody.create(prettyResponseBody, contentType);
				Response.Builder responseBuilder = response.newBuilder();
				response = responseBuilder
						.body(responseBody)
						.build();
			} catch (Exception e) {
				Log.e(TAG, "Error in networkCall: " + e);
				throw e;
			}

		}
		return response;
	}

}
