package it.ildevid.markers.network.api;

import it.ildevid.markers.network.dto.DtoAppRequest;
import it.ildevid.markers.network.dto.DtoAppResponse;
import it.ildevid.markers.network.dto.DtoSavePoiRequest;
import it.ildevid.markers.network.dto.DtoSaveRouteRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ICoreAPI {

    @POST("services/rest/savePoi")
    Call<DtoAppResponse<Void>> savePoi(@Body DtoAppRequest<DtoSavePoiRequest> request);

    @POST("services/rest/saveRoute")
    Call<DtoAppResponse<Void>> saveRoute(@Body DtoAppRequest<DtoSaveRouteRequest> request);

}
