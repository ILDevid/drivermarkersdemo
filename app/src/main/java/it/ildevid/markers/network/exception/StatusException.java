package it.ildevid.markers.network.exception;

public class StatusException extends Exception {

    String status;

    public StatusException(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
