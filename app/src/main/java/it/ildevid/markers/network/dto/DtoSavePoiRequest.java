package it.ildevid.markers.network.dto;

import java.io.Serializable;

public class DtoSavePoiRequest implements Serializable {

	private String id;
	private String image;
	private String note;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
