package it.ildevid.markers.network;

import androidx.annotation.StringDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class NetworkResponseStatus implements Serializable {

	@StringDef({SUCCESS, GENERIC_ERROR})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Status{}

	public static final String SUCCESS = "SUCCESS";
	public static final String GENERIC_ERROR = "GENERIC_ERROR";
	//TODO aggiungere altri tipi di errore

}
