package it.ildevid.markers.network.exception;

import androidx.annotation.Nullable;

public class HttpException extends Exception {

    String message;
    int code;

    public HttpException(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public int getHttpCode() {
        return code;
    }


    public String getHttpMessage(){
        return message;
    }

    @Nullable
    @Override
    public String getMessage() {
        return "message='" + message + '\'' + ", code=" + code;
    }


}
