package it.ildevid.markers;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import it.ildevid.markers.databinding.ActivityMainBinding;
import it.ildevid.markers.viewmodel.MapViewModel;
import it.ildevid.markers.viewmodel.WindowViewModel;
import it.ildevid.markers.widget.CustomProgressDialogFragment;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = MainActivity.class.getSimpleName();

	private ActivityMainBinding binding;

	private MapViewModel mapViewModel;

	private CustomProgressDialogFragment loader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityMainBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
		WindowViewModel windowViewModel = new ViewModelProvider(this).get(WindowViewModel.class);

		mapViewModel.restoreSaveInstanceState(savedInstanceState);

		windowViewModel.getLoader().observe(this, integer -> {
			if (loader == null) {
				loader = new CustomProgressDialogFragment();
			}
			if (integer >= 1) {
				Log.d(TAG, "TRY Loader show");
				if (!loader.isVisible()) {
					Log.d(TAG, "Loader show");
					loader.showNow(getSupportFragmentManager(), "");
				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		binding = null;
	}

	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		mapViewModel.onSaveInstanceState(outState);
	}

}