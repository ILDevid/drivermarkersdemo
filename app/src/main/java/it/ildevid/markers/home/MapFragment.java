package it.ildevid.markers.home;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import it.ildevid.markers.R;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.utils.NavHelper;
import it.ildevid.markers.viewmodel.MapViewModel;

public abstract class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

	private static final String TAG = MapFragment.class.getSimpleName();

	protected GoogleMap mMap;

	MapViewModel mapViewModel;

	protected static final int REQUEST_CODE_LOCATION = 1000;

	abstract void onPoiAdded(PoiData poiData, Marker marker);

	abstract void onMarkerClicked(Marker marker);

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mapViewModel = new ViewModelProvider(requireActivity()).get(MapViewModel.class);
	}

	@SuppressLint("MissingPermission")
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;

		try {
			boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style));
			if (!success) {
				Log.e(TAG, "Style parsing failed.");
			}
		} catch (Resources.NotFoundException e) {
			Log.e(TAG, "Can't find style. Error: ", e);
		}
	}

	public void animateCamera(LocationData location) {
		if (mMap != null && location != null) {
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			if (mapViewModel.getNavigationActive().get() && isNavigationFragmentActive()) {
				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(latLng)
						.tilt(60)
						.zoom(19)
						.bearing(0)
						.build();
				mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
			} else {
				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(latLng)
						.tilt(0)
						.zoom(17)
						.bearing(0)
						.build();
				mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
				//mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
			}
		}
	}

	public void moveCamera(LocationData location) {
		if (mMap != null && location != null) {
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
		}
	}

	public void addMarker(PoiData poiData, boolean showOnMap) {
		if (mMap != null && poiData != null && poiData.getLocation() != null) {
			Marker marker = null;
			if (showOnMap) {
				marker = mMap.addMarker(new MarkerOptions()
						.position(new LatLng(poiData.getLocation().getLatitude(), poiData.getLocation().getLongitude()))
						.title("Wrong Turn!")
						.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
				);
				marker.setTag(poiData.getLocation().getLatitude() + poiData.getLocation().getLongitude());
				mMap.setOnMarkerClickListener(this);
			}
			onPoiAdded(poiData, marker);
		}
	}

	protected void clearMapPolylines() {
		if (mMap != null) {
			mMap.clear();
			animateCamera(mapViewModel.getLastLocation());
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		onMarkerClicked(marker);
		return true;
	}

	private boolean isNavigationFragmentActive() {
		return NavHelper.getForegroundFragment((AppCompatActivity) requireActivity()) instanceof HomeFragment;
	}

}
