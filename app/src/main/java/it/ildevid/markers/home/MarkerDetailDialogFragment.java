package it.ildevid.markers.home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import it.ildevid.markers.R;
import it.ildevid.markers.databinding.FragmentMarkerDetailBinding;
import it.ildevid.markers.utils.NavHelper;
import it.ildevid.markers.viewmodel.MapViewModel;
import it.ildevid.markers.viewmodel.WindowViewModel;
import it.ildevid.markers.widget.ExpandedBottomSheetFragment;
import pl.aprilapps.easyphotopicker.ChooserType;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.MediaFile;
import pl.aprilapps.easyphotopicker.MediaSource;

import static android.view.View.SCROLLBAR_POSITION_RIGHT;

public class MarkerDetailDialogFragment extends ExpandedBottomSheetFragment {

	private static final String TAG = MarkerDetailDialogFragment.class.getSimpleName();

	private FragmentMarkerDetailBinding binding;

	private MapViewModel mapViewModel;
	private WindowViewModel windowViewModel;

	private EasyImage imagePicker;

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = FragmentMarkerDetailBinding.inflate(inflater, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mapViewModel = new ViewModelProvider(requireActivity()).get(MapViewModel.class);
		windowViewModel = new ViewModelProvider(requireActivity()).get(WindowViewModel.class);

		binding.setPoiData(mapViewModel.getCurrentPoi());

		boolean editMode = MarkerDetailDialogFragmentArgs.fromBundle(requireArguments()).getEditMode();
		binding.setEditMode(editMode);

		float cornerRadius = getResources().getDimension(R.dimen.default_corner_radius);
		binding.cardImage.setShapeAppearanceModel(
				new ShapeAppearanceModel().toBuilder()
						.setTopLeftCorner(CornerFamily.ROUNDED, cornerRadius)
						.setTopRightCorner(CornerFamily.ROUNDED, cornerRadius)
						.build()
		);

		/*initEditTextMultiLineMode(3,
				EditorInfo.IME_FLAG_NO_ENTER_ACTION,
				false,
				OVER_SCROLL_ALWAYS,
				SCROLLBARS_INSIDE_INSET,
				true);*/

		if (editMode) {
			binding.cardImage.setOnClickListener(v -> {
				imagePicker = new EasyImage.Builder(requireContext())
						.setChooserType(ChooserType.CAMERA_AND_GALLERY)
						.setCopyImagesToPublicGalleryFolder(false)
						.allowMultiple(false)
						.build();
				imagePicker.openChooser(this);
			});

			binding.buttonConfirm.setOnClickListener(v -> {
				windowViewModel.showLoader();
				mapViewModel.savePoi().observe(getViewLifecycleOwner(), result -> {
					windowViewModel.hideLoader();
					String message;
					if (result) {
						message = "POI salvato";
					} else {
						message = "Errore durante il salvataggio del POI";
					}
					Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
					NavHelper.popBackStack(getActivity());
				});
			});
		}

		if (mapViewModel.getCurrentPoi() != null && mapViewModel.getCurrentPoi().getImage() != null) {
			RequestOptions requestOptions = new RequestOptions()
					.centerCrop()
					.transform(new CenterCrop(), new GranularRoundedCorners(cornerRadius, cornerRadius, 0, 0));
			Glide.with(requireActivity())
					.load(mapViewModel.getCurrentPoi().getImage())
					.apply(requestOptions)
					.into(binding.imageMarkerPhoto);
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		binding = null;
		mapViewModel.resetCurrentPoi();
	}

	@SuppressLint("ClickableViewAccessibility")
	public void initEditTextMultiLineMode(int maxLines, int imeOptions, boolean singleLine, int overScrollMode, int scrollbarStyle, boolean verticalScrollBarEnabled) {
		binding.editTextNote.setImeOptions(imeOptions);
		binding.editTextNote.setSingleLine(singleLine);
		binding.editTextNote.setOverScrollMode(overScrollMode);
		if (verticalScrollBarEnabled) {
			binding.editTextNote.setScroller(new Scroller(getContext()));
			binding.editTextNote.setVerticalScrollBarEnabled(true);
			binding.editTextNote.setScrollBarStyle(scrollbarStyle);
			binding.editTextNote.setVerticalScrollbarPosition(SCROLLBAR_POSITION_RIGHT);
		}
		binding.editTextNote.setMaxLines(maxLines);
		binding.editTextNote.setOnTouchListener((v, event) -> {
			if (binding.editTextNote.hasFocus()) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_SCROLL) {
					v.getParent().requestDisallowInterceptTouchEvent(false);
					return true;
				}
			}
			return false;
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (imagePicker != null) {
			imagePicker.handleActivityResult(requestCode, resultCode, data, requireActivity(), new DefaultCallback() {
				@Override
				public void onMediaFilesPicked(@NonNull MediaFile[] imageFiles, @NonNull MediaSource source) {
					if (imageFiles.length > 0 && imageFiles[0] != null) {
						Log.d(TAG, "on media picked success");
						loadPickedImage(imageFiles[0]);
					}
				}

				@Override
				public void onImagePickerError(@NonNull Throwable error, @NonNull MediaSource source) {
					Log.e(TAG, "Error in imagePicker handle result: " + error.getMessage());
				}

				@Override
				public void onCanceled(@NonNull MediaSource source) {
					//Not necessary to remove any files manually anymore
				}
			});
		}
	}

	private void loadPickedImage(MediaFile imageFile) {
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getFile().getAbsolutePath(), bmOptions);
		bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 3, bitmap.getHeight() / 3, true);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);
		Bitmap bitmapCompressed = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
		//binding.imageMarkerPhoto.setImageBitmap(bitmap);
		Log.d(TAG, "Image bitmap width = " + bitmap.getWidth() + " - height = " + bitmap.getHeight());
		int cornerRadius = (int) getResources().getDimension(R.dimen.default_corner_radius);
		RequestOptions requestOptions = new RequestOptions()
				.centerCrop()
				.transform(new CenterCrop(), new GranularRoundedCorners(cornerRadius, cornerRadius, 0, 0));
		Glide.with(requireContext())
				.load(bitmapCompressed)
				.apply(requestOptions)
				.into(binding.imageMarkerPhoto);
		mapViewModel.getCurrentPoi().setImage(bitmapCompressed);
	}

}
