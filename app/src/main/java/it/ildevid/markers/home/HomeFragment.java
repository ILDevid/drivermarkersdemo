package it.ildevid.markers.home;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.Observable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import it.ildevid.markers.R;
import it.ildevid.markers.databinding.FragmentHomeBinding;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.utils.Constants;
import it.ildevid.markers.utils.NavHelper;
import it.ildevid.markers.utils.PreferenceHelper;
import it.ildevid.markers.viewmodel.WindowViewModel;
import it.ildevid.markers.widget.ToolbarCustom;

public class HomeFragment extends LocationFragment {

	private FragmentHomeBinding binding;

	private WindowViewModel windowViewModel;

	private Observable.OnPropertyChangedCallback navigationChangedCallback;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		binding = FragmentHomeBinding.inflate(inflater, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		windowViewModel = new ViewModelProvider(requireActivity()).get(WindowViewModel.class);

		binding.toolbar.setToolbarListener(new ToolbarCustom.ToolbarListener() {
			@Override
			public void onBackPressed(View v) {
				//
			}

			@Override
			public void onMenuPressed(View v) {
				NavHelper.navigate(getActivity(), HomeFragmentDirections.actionHomeFragmentToMarkerListFragment());
			}
		});

		SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.support_map);
		if (mapFragment != null) {
			mapFragment.getMapAsync(this);
		}

		navigationChangedCallback = new Observable.OnPropertyChangedCallback() {
			@Override
			public void onPropertyChanged(Observable sender, int propertyId) {
				boolean navigationActive = mapViewModel.getNavigationActive().get();
				if (navigationActive) {
					//binding.fabAddMarker.hide();
				} else {
					//binding.fabAddMarker.show();
					showSaveRouteDialog();
				}
			}
		};

		mapViewModel.getNavigationActive().addOnPropertyChangedCallback(navigationChangedCallback);
		binding.setNavigationActive(mapViewModel.getNavigationActive());

		binding.fabAddMarker.setOnClickListener(v -> addMarker(mapViewModel.getCurrentPoi(), false));

		binding.fabNavigate.setOnClickListener(v -> mapViewModel.toggleNavigationActive());
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mapViewModel.getNavigationActive().removeOnPropertyChangedCallback(navigationChangedCallback);
		binding = null;
	}

	@Override
	void onMapReady() {
		//mMap.setOnCameraMoveListener(() -> binding.fabAddMarker.shrink());
		//mMap.setOnCameraIdleListener(() -> binding.fabAddMarker.extend());

		LocationData lastLocation = PreferenceHelper.getInstance().getLastLocationData(requireContext());
		if (lastLocation != null) {
			LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
		}
	}

	@Override
	void onLocationUpdate(LocationData location) {
		//Not used
	}

	@Override
	void onPoiAdded(PoiData poiData, Marker marker) {
		mapViewModel.getCurrentPoi().setLocation(poiData.getLocation());
		NavHelper.navigate(getActivity(),
				HomeFragmentDirections.actionHomeFragmentToMarkerDetailDialogFragment().setEditMode(true));
	}

	@Override
	void onMarkerClicked(Marker marker) {
		//Not used
	}

	private void showSaveRouteDialog() {
		MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
		builder.setTitle("Fine navigazione")
				.setMessage("Vuoi salvare il percorso tracciato?")
				.setCancelable(false)
				.setPositiveButton("Salva", (dialog, which) -> {
					stopRouteTracking(true);
					clearMapPolylines();
				})
				.setNegativeButton("Annulla", (dialog, which) -> {
					stopRouteTracking(false);
					clearMapPolylines();
				});

		AlertDialog alertDialog = builder.create();

		alertDialog.setOnShowListener(dialog -> {
			alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
					.setTextColor(ContextCompat.getColor(requireContext(), R.color.redMarker));
			alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
					.setTextColor(ContextCompat.getColor(requireContext(), R.color.redMarker));
		});

		alertDialog.show();
	}

	private void stopRouteTracking(boolean saveRoute) {
		if (Constants.SYNCHRONIZATION_ENABLED) {
			windowViewModel.showLoader();
		}
		mapViewModel.stopRouteTrackingAndSaveData(saveRoute).observe(getViewLifecycleOwner(), result -> {
			if (Constants.SYNCHRONIZATION_ENABLED) {
				windowViewModel.hideLoader();
			}
			if (result) {
				Toast.makeText(requireContext(), "Percorso salvato", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(requireContext(), "Errore durante il salvataggio del percorso", Toast.LENGTH_SHORT).show();
			}
		});
	}

}
