package it.ildevid.markers.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import it.ildevid.markers.R;
import it.ildevid.markers.databinding.FragmentMarkerListBinding;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.utils.NavHelper;
import it.ildevid.markers.utils.PreferenceHelper;
import it.ildevid.markers.viewmodel.MapViewModel;
import it.ildevid.markers.widget.ToolbarCustom;

public class MarkerListFragment extends MapFragment {

	private FragmentMarkerListBinding binding;

	private MapViewModel mapViewModel;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		binding = FragmentMarkerListBinding.inflate(inflater, container, false);
		return binding.getRoot();
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mapViewModel = new ViewModelProvider(requireActivity()).get(MapViewModel.class);

		SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.support_map);
		if (mapFragment != null) {
			mapFragment.getMapAsync(this);
		}

		binding.toolbar.setToolbarListener(new ToolbarCustom.ToolbarListener() {
			@Override
			public void onBackPressed(View v) {
				NavHelper.popBackStack(getActivity());
			}

			@Override
			public void onMenuPressed(View v) {
				//Not used
			}
		});

		loadMarkerList();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		binding = null;
	}

	@SuppressLint("MissingPermission")
	@Override
	public void onMapReady(GoogleMap googleMap) {
		super.onMapReady(googleMap);

		LocationData lastLocation = PreferenceHelper.getInstance().getLastLocationData(requireContext());
		if (lastLocation != null) {
			LatLng latLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
		}
	}

	@Override
	void onPoiAdded(PoiData poiData, @Nullable Marker marker) {
		//Not used
	}

	@Override
	void onMarkerClicked(Marker marker) {
		moveCamera(new LocationData(marker.getPosition()));
		mapViewModel.setCurrentPoi(mapViewModel.getPoiMap().get(marker.getTag()));
		NavHelper.navigate(getActivity(),
				MarkerListFragmentDirections.actionMarkerListFragmentToMarkerDetailDialogFragment().setEditMode(false)
		);
	}

	private void loadMarkerList() {
		//binding.toolbar.setTitle("Caricamento POIs...");
		mapViewModel.getPoiList().observe(getViewLifecycleOwner(), poiList -> {
			for (PoiData item : poiList) {
				addMarker(item, true);
			}
			if (poiList.isEmpty()) {
				Toast.makeText(requireContext(), "Nessun POI salvato", Toast.LENGTH_SHORT).show();
			} else {
				animateCamera(poiList.get(poiList.size() - 1).getLocation());
			}
			//binding.toolbar.setTitle("Lista POI");
		});
	}

}
