package it.ildevid.markers.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import it.ildevid.markers.R;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.utils.PreferenceHelper;

import static android.content.Context.LOCATION_SERVICE;
import static it.ildevid.markers.utils.Constants.DISPLACEMENT;
import static it.ildevid.markers.utils.Constants.FASTEST_INTERVAL;
import static it.ildevid.markers.utils.Constants.UPDATE_INTERVAL;

public abstract class LocationFragment extends MapFragment implements OnMapReadyCallback {

	private static final String TAG = LocationFragment.class.getSimpleName();

	private LocationCallback googleLocationCallback;

	private boolean isPermissionLocationServicesRequestedAlready;

	private static final int REQUEST_CODE_RESOLUTION = 2000;

	abstract void onMapReady();

	abstract void onPoiAdded(PoiData poiData, Marker marker);

	abstract void onLocationUpdate(LocationData location);

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		isPermissionLocationServicesRequestedAlready = false;

		googleLocationCallback = new LocationCallback() {
			@Override
			public void onLocationResult(@NonNull LocationResult locationResult) {
				Log.d(TAG, "onLocationResult = " + locationResult.getLastLocation().toString());
				LocationData currentLocation = new LocationData();
				currentLocation.setLatitude(locationResult.getLastLocation().getLatitude());
				currentLocation.setLongitude(locationResult.getLastLocation().getLongitude());

				mapViewModel.setLastLocation(currentLocation);

				animateCamera(currentLocation);
				onLocationUpdate(currentLocation);
			}
		};

	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mapViewModel.getMapRouteLiveData().observe(getViewLifecycleOwner(), routePoint -> {
			if (mMap != null) {
				mapViewModel.addPointToCurrentPath(routePoint.getCurrentLocation());
				if (routePoint.getPreviousLocation() != null) {
					Polyline polyline = mMap.addPolyline(new PolylineOptions()
							.add(new LatLng(routePoint.getPreviousLocation().getLatitude(), routePoint.getPreviousLocation().getLongitude()),
									new LatLng(routePoint.getCurrentLocation().getLatitude(), routePoint.getCurrentLocation().getLongitude()))
							.width(18)
							.color(ContextCompat.getColor(requireContext(), R.color.redMarker))
							.geodesic(false)
					);
					polyline.setStartCap(new RoundCap());
					polyline.setEndCap(new RoundCap());
					polyline.setJointType(JointType.ROUND);
				}

				Log.d(TAG, "Draw polyline on location: " + routePoint.getCurrentLocation().toString());
			}
		});
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LocationServices.getFusedLocationProviderClient(requireContext()).removeLocationUpdates(googleLocationCallback);
	}

	@SuppressLint("MissingPermission")
	@Override
	public void onMapReady(GoogleMap googleMap) {
		super.onMapReady(googleMap);

		if (hasLocationPermission()) {
			mMap.setMyLocationEnabled(true);
			startLocationUpdates();
		} else {
			requestLocationPermission();
		}

		onMapReady();
	}

	@SuppressLint("MissingPermission")
	protected void startLocationUpdates() {
		Log.d(TAG, "startLocationUpdates");

		LocationRequest mLocationRequest =
				LocationRequest.create()
						.setInterval(UPDATE_INTERVAL)
						.setFastestInterval(FASTEST_INTERVAL)
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
						.setSmallestDisplacement(DISPLACEMENT)
						.setWaitForAccurateLocation(true);

		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
				.addLocationRequest(mLocationRequest);

		builder.setAlwaysShow(true);

		SettingsClient settingsClient = LocationServices.getSettingsClient(requireContext());
		Task<LocationSettingsResponse> result = settingsClient.checkLocationSettings(builder.build());
		result.addOnCompleteListener(task -> {
			try {
				if (!isPermissionLocationServicesRequestedAlready) {
					isPermissionLocationServicesRequestedAlready = true;

					LocationSettingsResponse response = task.getResult(ApiException.class);
					/*if (response != null) {
						if (response.getLocationSettingsStates() != null && response.getLocationSettingsStates().isLocationUsable()
								&& !DeviceUtils.isDeviceOnline(requireContext())) {
							//
						}
					}*/
					Log.d(TAG, "LocationSettingsResponse = " + (response != null ? response.toString() : "null"));
				}

				//startLocationUpdates();
			} catch (ApiException exception) {
				switch (exception.getStatusCode()) {
					case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
						try {
							ResolvableApiException resolvable = (ResolvableApiException) exception;
							resolvable.startResolutionForResult(requireActivity(), REQUEST_CODE_RESOLUTION);
						} catch (IntentSender.SendIntentException | ClassCastException e) {
							//Ignore
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						break;
				}
			}
		});

		FusedLocationProviderClient locationClient = LocationServices.getFusedLocationProviderClient(requireContext());
		locationClient.getLastLocation()
				.addOnSuccessListener(new OnSuccessListener<Location>() {
					@Override
					public void onSuccess(Location location) {
						// GPS location can be null if GPS is switched off
						if (location != null) {
							LocationData lastLocation = new LocationData();
							lastLocation.setLatitude(location.getLatitude());
							lastLocation.setLongitude(location.getLongitude());
							Log.d(TAG, "Lat: " + location.getLatitude() + " Long: " + location.getLongitude());

							//mapViewModel.setLastLocation(lastLocation);
							PreferenceHelper.getInstance().putLastLocationData(requireContext(), lastLocation);
						} else {

							if (hasLocationPermission()) {

								LocationManager locationManager = (LocationManager) requireActivity().getSystemService(LOCATION_SERVICE);
								if (locationManager != null) {
									locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, FASTEST_INTERVAL, 0, new LocationListener() {
										@Override
										public void onLocationChanged(Location location) {
											Log.d(TAG, "NETWORK_PROVIDER onLocationChanged = " + location);
											locationManager.removeUpdates(this);
										}

										@Override
										public void onStatusChanged(String provider, int status, Bundle extras) {
										}

										@Override
										public void onProviderEnabled(String provider) {
										}

										@Override
										public void onProviderDisabled(String provider) {
										}
									});
								}

							}

						}
					}
				})
				.addOnFailureListener(e ->
						Log.d(TAG, "Error trying to get last GPS location: " + e.getMessage()));

		LocationServices.getFusedLocationProviderClient(requireContext())
				.requestLocationUpdates(mLocationRequest, googleLocationCallback, Looper.myLooper());
	}

	protected boolean hasLocationPermission() {
		return ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
	}

	protected void requestLocationPermission() {
		requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
	}

	@SuppressLint("MissingPermission")
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == REQUEST_CODE_LOCATION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				mMap.setMyLocationEnabled(true);
				startLocationUpdates();
			} else {
				if (!isPermissionLocationServicesRequestedAlready) {
					showPermissionDeniedWarningDialog();
				} else {
					Toast.makeText(requireContext(),
							"Per utilizzare correttamente l'app concedi il permesso dalle impostazioni di sistema",
							Toast.LENGTH_LONG)
							.show();
				}
			}
		}
	}

	private void showPermissionDeniedWarningDialog() {
		MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
		builder.setTitle("Attenzione")
				.setMessage("Non puoi usare correttamente l'applicazione senza concedere il permesso di accesso alla posizione, vuoi riprovare?")
				.setCancelable(false)
				.setPositiveButton("Riprova", (dialog, which) -> requestLocationPermission())
				.setNegativeButton("Esci", (dialog, which) -> requireActivity().finish());

		AlertDialog alertDialog = builder.create();

		alertDialog.setOnShowListener(dialog -> {
			isPermissionLocationServicesRequestedAlready = true;
			alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
					.setTextColor(ContextCompat.getColor(requireContext(), R.color.redMarker));
			alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
					.setTextColor(ContextCompat.getColor(requireContext(), R.color.redMarker));
		});

		alertDialog.show();
	}

}
