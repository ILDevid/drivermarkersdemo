package it.ildevid.markers.widget;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import it.ildevid.markers.R;
import it.ildevid.markers.databinding.WidgetsProgressDialogTransparentBinding;
import it.ildevid.markers.viewmodel.WindowViewModel;

public class CustomProgressDialogFragment extends DialogFragment {

    private WidgetsProgressDialogTransparentBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_FullScreenProgressDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = WidgetsProgressDialogTransparentBinding.inflate(inflater, container, false);
        requireDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WindowViewModel windowViewModel = new ViewModelProvider(requireActivity()).get(WindowViewModel.class);

        windowViewModel.getLoader().observe(getViewLifecycleOwner(), integer -> {
            if (integer == 0) {
                dismiss();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
