package it.ildevid.markers.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import it.ildevid.markers.R;
import it.ildevid.markers.databinding.WidgetToolbarCustomBinding;

public class ToolbarCustom extends LinearLayout {

	private final WidgetToolbarCustomBinding binding;

	public interface ToolbarListener {
		void onBackPressed(View v);

		void onMenuPressed(View v);
	}

	public ToolbarCustom(final Context context, AttributeSet attrs) {
		super(context, attrs);
		binding = WidgetToolbarCustomBinding.inflate(LayoutInflater.from(context), this, true);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ToolbarCustom);

		binding.imageBack.setVisibility(a.getBoolean(R.styleable.ToolbarCustom_backVisible, false) ? VISIBLE : INVISIBLE);
		binding.imageMenu.setVisibility(a.getBoolean(R.styleable.ToolbarCustom_menuVisible, false) ? VISIBLE : INVISIBLE);
		binding.textTitle.setText(a.getString(R.styleable.ToolbarCustom_title));

		a.recycle();
	}

	public void setToolbarListener(ToolbarListener toolbarListener) {
		binding.setToolbarListener(toolbarListener);
	}

	public void setTitle(String title) {
		binding.textTitle.setText(title);
	}

}
