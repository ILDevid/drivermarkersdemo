package it.ildevid.markers.callable;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import it.ildevid.markers.DriverMarkersApplication;
import it.ildevid.markers.database.DrivingMarkerDatabase;
import it.ildevid.markers.database.entity.PoiEntity;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.model.PoiData;

public class GetPoiListTask implements Callable<ArrayList<PoiData>> {

	private static final String TAG = GetPoiListTask.class.getSimpleName();

	@Override
	public ArrayList<PoiData> call() {

		ArrayList<PoiData> poiList = new ArrayList<>();

		try {

			List<PoiEntity> poiEntities = DrivingMarkerDatabase.getInstance(DriverMarkersApplication.getContext()).getPoiDao().getPoiList();
			Log.d(TAG, "Saved poi number = " + poiEntities.size());

			for (PoiEntity item : poiEntities) {
				PoiData data = new PoiData();
				data.setId(item.getPoiId());
				data.setImage(item.getImageBase64());
				data.setNote(item.getNote());
				LocationData location = new LocationData();
				location.setLatitude(item.getLatitude());
				location.setLongitude(item.getLongitude());
				data.setLocation(location);

				poiList.add(data);
			}

		} catch (Exception e) {
			Log.e(TAG, "GetPoiListTask error: " + e.getMessage());
		}

		return poiList;
	}

}
