package it.ildevid.markers.callable;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import it.ildevid.markers.DriverMarkersApplication;
import it.ildevid.markers.database.DrivingMarkerDatabase;
import it.ildevid.markers.database.entity.RouteEntity;
import it.ildevid.markers.model.LocationData;
import it.ildevid.markers.network.Network;
import it.ildevid.markers.network.api.CoreAPI;
import it.ildevid.markers.network.dto.DtoAppRequest;
import it.ildevid.markers.network.dto.DtoAppResponse;
import it.ildevid.markers.network.dto.DtoLocation;
import it.ildevid.markers.network.dto.DtoSaveRouteRequest;

import static it.ildevid.markers.network.NetworkResponseStatus.SUCCESS;

public class SaveRouteTask implements Callable<Boolean> {

	private static final String TAG = SaveRouteTask.class.getSimpleName();

	private final ArrayList<LocationData> routePoints;
	private final boolean synchronizeData;

	public SaveRouteTask(@NonNull ArrayList<LocationData> routePoints, boolean synchronizeData) {
		this.routePoints = routePoints;
		this.synchronizeData = synchronizeData;
	}

	@Override
	public Boolean call() throws Exception {

		long insertedRouteId = -1;

		try {
			RouteEntity routeEntity = new RouteEntity();
			routeEntity.setRoutePoints(routePoints);

			insertedRouteId = DrivingMarkerDatabase.getInstance(DriverMarkersApplication.getContext()).getRouteDao().saveRoute(routeEntity);
			Log.d(TAG, "Saved route with id = " + insertedRouteId);

		} catch (Exception e) {
			Log.e(TAG, "SaveRouteTask error: " + e.getMessage());
		}

		if (synchronizeData) {

			DtoSaveRouteRequest dtoRequest = new DtoSaveRouteRequest();
			dtoRequest.setId(String.valueOf(insertedRouteId));
			List<DtoLocation> pointList = new ArrayList<>();
			for (LocationData item : routePoints) {
				DtoLocation dtoLocation = new DtoLocation();
				dtoLocation.setLatitude(String.valueOf(item.getLatitude()));
				dtoLocation.setLongitude(String.valueOf(item.getLongitude()));
				pointList.add(dtoLocation);
			}
			dtoRequest.setPoints(pointList);

			DtoAppRequest<DtoSaveRouteRequest> request = new DtoAppRequest<>(dtoRequest);

			DtoAppResponse<Void> response = new Network<>(CoreAPI.getServerAPI().saveRoute(request)).call();
			if (response.getStatus().equals(SUCCESS)) {
				Log.d(TAG, "SavePoiTask synchronization success!");
			} else {
				Log.e(TAG, "SavePoiTask synchronization error: " + response.getStatus());
			}
		}

		return insertedRouteId != -1;
	}

}
