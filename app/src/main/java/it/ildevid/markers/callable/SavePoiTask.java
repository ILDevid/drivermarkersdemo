package it.ildevid.markers.callable;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.concurrent.Callable;

import it.ildevid.markers.DriverMarkersApplication;
import it.ildevid.markers.database.DrivingMarkerDatabase;
import it.ildevid.markers.database.entity.PoiEntity;
import it.ildevid.markers.model.PoiData;
import it.ildevid.markers.network.Network;
import it.ildevid.markers.network.api.CoreAPI;
import it.ildevid.markers.network.dto.DtoAppRequest;
import it.ildevid.markers.network.dto.DtoAppResponse;
import it.ildevid.markers.network.dto.DtoSavePoiRequest;
import it.ildevid.markers.utils.BitmapUtils;

import static it.ildevid.markers.network.NetworkResponseStatus.SUCCESS;

public class SavePoiTask implements Callable<Boolean> {

	private static final String TAG = SavePoiTask.class.getSimpleName();

	private final PoiData poiData;
	private final boolean synchronizeData;

	public SavePoiTask(@NonNull PoiData poiData, boolean synchronizeData) {
		this.poiData = poiData;
		this.synchronizeData = synchronizeData;
	}

	@Override
	public Boolean call() throws Exception {

		long insertedPoiId = -1;

		try {
			PoiEntity poiEntity = new PoiEntity();
			poiEntity.setImageBase64(BitmapUtils.bitmapToBase64(poiData.getImage()));
			poiEntity.setNote(poiData.getNote());
			poiEntity.setLatitude(poiData.getLocation().getLatitude());
			poiEntity.setLongitude(poiData.getLocation().getLongitude());

			insertedPoiId = DrivingMarkerDatabase.getInstance(DriverMarkersApplication.getContext()).getPoiDao().savePoi(poiEntity);
			Log.d(TAG, "Saved poi with id = " + insertedPoiId);

		} catch (Exception e) {
			Log.e(TAG, "SavePoiTask error: " + e.getMessage());
		}

		if (synchronizeData) {

			DtoSavePoiRequest dtoRequest = new DtoSavePoiRequest();
			dtoRequest.setId(String.valueOf(insertedPoiId));
			dtoRequest.setImage(BitmapUtils.bitmapToBase64(poiData.getImage()));
			dtoRequest.setNote(poiData.getNote());
			DtoAppRequest<DtoSavePoiRequest> request = new DtoAppRequest<>(dtoRequest);

			DtoAppResponse<Void> response = new Network<>(CoreAPI.getServerAPI().savePoi(request)).call();
			if (response.getStatus().equals(SUCCESS)) {
				Log.d(TAG, "SavePoiTask synchronization success!");
			} else {
				Log.d(TAG, "SavePoiTask synchronization error: " + response.getStatus());
			}

		}

		return insertedPoiId != -1;
	}

}
